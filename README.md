# trabajo_paper_modulo_2

Codigo para el trabajo del segundo modulo del diplomado Gabriel Casas Mamani.

Los modelos se encuentran serializados en los archivos **sgdModel.pkl** y **svmModel.pkl**

el archivo que los ejecuta es: **svm_sgd.py**
para serializar el dataset se tiene archivo **dataToPickle.py**
el data set serializado se emplea en el archivo **fitModel.py** para entrenar los modelos.